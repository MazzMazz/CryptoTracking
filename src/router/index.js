import Vue from 'vue'
import Router from 'vue-router'

// Containers
import Full from '@/containers/Full'

// Views
import Dashboard from '@/views/Dashboard'
import AddContract from '@/views/AddContract'
import Ethereum from '@/views/Ethereum'
import ContractDetail from '@/views/Contract'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/dashboard',
      name: 'Home',
      component: Full,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        {
          path: 'add-contract',
          name: 'Add Contract',
          component: AddContract
        },
        {
          path: 'ethereum',
          name: 'Ethereum',
          component: Ethereum
        },
        {
          path: 'contract/:address',
          name: 'ContractDetail',
          component: ContractDetail
        }
      ]
    }
  ]
})
