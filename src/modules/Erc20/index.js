import Web3 from 'web3'
import erc20abi from './abi/erc20abi'

class Erc20SmartContract {
  constructor (smartContractAddress, decimals) {
    Erc20SmartContract._initWeb3()

    this.contract = new Erc20SmartContract.web3.eth.Contract(erc20abi,
      smartContractAddress)
    this.contractWs = new Erc20SmartContract.web3Ws.eth.Contract(erc20abi,
      smartContractAddress)
    this.decimals = decimals
  }

  getBalanceOfAccount (walletAddress) {
    return new Promise((resolve, reject) => {
      this.contract.methods.balanceOf(walletAddress).call((error, data) => {
        if (error) {
          reject(error)
        }
        else {
          resolve(this.getHumanReadableValue(data))
        }
      })
    })
  }

  static isValidAddress (address) {
    return Erc20SmartContract.web3.utils.isAddress(address)
  }

  onTransfer (successCB, errorCB) {
    this.contractWs.events.Transfer({}, (error, event) => {
      if (error) {
        errorCB(error)
      }
      else {
        successCB({
          blockNumber: event.blockNumber,
          event: event.event,
          from: event.returnValues.from,
          to: event.returnValues.to,
          amount: this.getHumanReadableValue(event.returnValues.value),
          transactionHash: event.transactionHash,
          blockHash: event.blockHash,
          contractAddress: event.address,
        })
      }
    })
  }

  getHumanReadableValue (value) {
    /*
     * smart Contracts have different amounts of decimals and we receive
     * the information without decimalinfo
     * e.g. 12 EOS would be 12 * 10^18 that is why we have to divide
     * by 10^18
     */
    return value / Math.pow(10, this.decimals)
  }

  static _initWeb3 () {
    if (!Erc20SmartContract.web3) {
      // TODO: move to env file?
      Erc20SmartContract.web3 = new Web3(
        'https://mainnet.infura.io/6EU6rKRAH1C4BfieDJ6q')
      Erc20SmartContract.web3Ws = new Web3('wss://mainnet.infura.io/ws')
    }
  }
}

export default Erc20SmartContract
