import BaseStorage from './BaseStorage'

class TransactionStorage {

  static TRANSACTION_STORAGE_KEY = 'TRANSACTION_STORAGE';


  static getAllTransactionsByAddress(address) {
    let transactions = BaseStorage.get(TransactionStorage.TRANSACTION_STORAGE_KEY)

    if(!transactions) {
      return [];
    }

    return transactions[address] ? transactions[address] : [];
  }

  static setTransactionsForAddress(address, newTransactions) {
    let transactions = BaseStorage.get(TransactionStorage.TRANSACTION_STORAGE_KEY)
    
    if (!transactions) {
      transactions = {}
    }
    
    transactions[address] = newTransactions;
    
    BaseStorage.set(TransactionStorage.TRANSACTION_STORAGE_KEY, transactions)
  }
}

export default TransactionStorage
