import BaseStorage from './BaseStorage'

class HistoryStorage {

    static HISTORY_STORAGE_KEY = 'HISTORY_STORAGE';

    static addEntryToHistory(contract, balance, transactionCount, date){
        let history = HistoryStorage.getHistoryForEthAccount(contract);

            history.push({
                transactionCount: transactionCount,
                balance: balance,
                date: date,
            });

        BaseStorage.set(HistoryStorage.HISTORY_STORAGE_KEY + contract, history)
    }

    static updateHistoryOfAccount (contract, balance, transactionCount, date) {
        let history = HistoryStorage.getHistoryForEthAccount(contract);

        if(history.length > 10) {
            history.shift();
        }

        if(history.length !== 0 && history[history.length-1].balance !== balance){
            history.push({
                transactionCount: transactionCount,
                balance: balance,
                date: date
            });
        }

        BaseStorage.set(HistoryStorage.HISTORY_STORAGE_KEY + contract, history)
    }

    static getRecentUpdateForEthAccount(contract){
        let history = BaseStorage.get(HistoryStorage.HISTORY_STORAGE_KEY + contract);
        return history[history.length - 1];
    }

    static getHistoryForEthAccount (contract) {
        let history = BaseStorage.get(HistoryStorage.HISTORY_STORAGE_KEY + contract);

        if (!history) {
            return [];
        }

        return history ? history : [];
    }
}

export default HistoryStorage
