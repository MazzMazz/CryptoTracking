import BaseStorage from './BaseStorage'

class BalanceStorage {

  static BALANCE_STORAGE_KEY = 'BALANCE_STORAGE';

  static updateBalanceOfWallet (contract, wallet, balance) {
    let balances = BalanceStorage.getAllWalletsByContract(contract)
    let updated = false;

    for (let i = 0; i < balances.length; i++) {
      if (balances[i].wallet === wallet) {
        balances[i].balance = balance;
        updated = true;
      }
    }

    if (!updated) {
      balances.push({
        balance: balance,
        wallet: wallet,
      });
    }

    BaseStorage.set(BalanceStorage.BALANCE_STORAGE_KEY + contract, balances)
  }

  static getAllWalletsByContract (contract) {
    let balances = BaseStorage.get(BalanceStorage.BALANCE_STORAGE_KEY + contract)

    if (!balances) {
      return [];
    }

    return balances ? balances : [];
  }

}

export default BalanceStorage
