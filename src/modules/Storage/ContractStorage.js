import BaseStorage from './BaseStorage'

class ContractStorage {

  static CONTRACT_STORAGE_KEY = 'CONTRACT_STORAGE';

  static addContract(name, address, decimals) {
    let contracts = ContractStorage.getAllContracts()

    contracts.push({
      id: Math.random().toString(36).substring(7),
      name: name,
      address: address,
      decimals: decimals
    })

    BaseStorage.set(ContractStorage.CONTRACT_STORAGE_KEY, contracts)
  }

  static getContractInfoByAddress(address) {
    let contracts = ContractStorage.getAllContracts()

    for(let i = 0; i < contracts.length; i++) {
      if(contracts[i].address === address) {
        return contracts[i];
      }
    }
  }

  static addressAlreadySaved(address) {
    let contracts = ContractStorage.getAllContracts()

    for(let i = 0; i < contracts.length; i++) {
      if(contracts[i].address === address) {
        return true;
      }
    }

    return false;
  }

  static getAllContracts() {
    let contracts = BaseStorage.get(ContractStorage.CONTRACT_STORAGE_KEY)

    if(!contracts) {
      return [];
    }

    return contracts;
  }
}

export default ContractStorage
