import axios from 'axios';

const SERVER = 'http://localhost:4000/graphql';
const ADDRESS = '0x32be343b94f860124dc4fee278fdcbd38c102d88';

class EthereumContract {
    constructor () {
        this.address = ADDRESS;
    }


    async getAccount () {
        let _this = this;
        try {
            const res = await axios.post(SERVER, {
                query: `
                query Account($address: String!) {
                    account(address: $address) {
                    	  address
	                      balance
	                      code
	                      type
	                      transactionCount
                    }
                }`,
                variables: {
                    address: _this.address
                }
            })
            return res.data.data.account
        } catch (e) {
            console.log('err', e)
            return 'ERROR:' + e;
        }
    }

    /*
    getBalanceOfAccount (walletAddress) {
        return new Promise((resolve, reject) => {
            this.contract.methods.balanceOf(walletAddress).call((error, data) => {
                if (error) {
                    reject(error)
                }
                else {
                    resolve(this.getHumanReadableValue(data))
                }
            })
        })
    }

    static isValidAddress (address) {
        return Erc20SmartContract.web3.utils.isAddress(address)
    }

    onTransfer (successCB, errorCB) {
        this.contractWs.events.Transfer({}, (error, event) => {
            if (error) {
                errorCB(error)
            }
            else {
                successCB({
                    blockNumber: event.blockNumber,
                    event: event.event,
                    from: event.returnValues.from,
                    to: event.returnValues.to,
                    amount: this.getHumanReadableValue(event.returnValues.value),
                    transactionHash: event.transactionHash,
                    blockHash: event.blockHash,
                    contractAddress: event.address,
                })
            }
        })
    }

    getHumanReadableValue (value) {
        /*
         * smart Contracts have different amounts of decimals and we receive
         * the information without decimalinfo
         * e.g. 12 EOS would be 12 * 10^18 that is why we have to divide
         * by 10^18
         */
    //    return value / Math.pow(10, this.decimals)
    // }
/*
    static _initWeb3 () {
        if (!Erc20SmartContract.web3) {
            // TODO: move to env file?
            Erc20SmartContract.web3 = new Web3(
                'https://mainnet.infura.io/6EU6rKRAH1C4BfieDJ6q')
            Erc20SmartContract.web3Ws = new Web3('wss://mainnet.infura.io/ws')
        }
    }
    */
}

export default EthereumContract
