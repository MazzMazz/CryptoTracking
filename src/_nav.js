export default {
  items: [
    {
      title: true,
      name: 'Menu',
    },
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'icon-speedometer'
    },
    {
      name: 'Add Contract',
      url: '/add-contract',
      icon: 'icon-plus'
    },
    {
      title: true,
      name: 'Your Contracts',
    },
    {
      name: 'Ethereum (default)',
      url: '/ethereum',
    }
  ]
}
