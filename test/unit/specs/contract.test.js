import ContractStorage from '../../../src/modules/Storage/ContractStorage';
import EthereumContract from '../../../src/modules/Ethereum';
const contractAdded = require('./contract');
const connection = require('./connection');

test('looks if contract is saved in localstorage', () => {
    let contractsOld = ContractStorage.getAllContracts();
    ContractStorage.addContract('EOS (EOS)', '0x86fa049857e0209aa7d9e616f7eb3b3b78ecfdb0', '18');
    let contractsNew = ContractStorage.getAllContracts();

    expect(contractAdded(contractsOld, contractsNew)).toBe(true);
});

test('looks if we can connect to graphql server', () => {
    let contract = new EthereumContract();
    return contract.getAccount().then((data) => {
       expect(data).toBeInstanceOf(Object);
    });
});