function connection (contract) {

    let returnValue = false;
    contract.getAccount().then(data => {

        console.log(data);
        returnValue = true;
    }, error => {});

    return returnValue;
}

module.exports = connection;
