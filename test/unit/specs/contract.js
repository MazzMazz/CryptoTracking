function contractAdded (a, b) { return b.length === a.length + 1; }

module.exports = contractAdded;
