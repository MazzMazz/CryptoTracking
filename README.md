# Blockchain monitoring tool

Our main goal of this project is a tool to monitor transactions, balances and events of a ERC20 Token on the ethereum blockchain

### Project requisites

For bringing our idea to life we need a few technologies

* vue.js for the reactive frontendhandling
* web3.js to connect tho the Ethereum Blockchain
* a blockchain hoster -> [Infura](https://infura.io/) - ETH
* Simple Admin Template -> [AdminTemplate](https://github.com/mrholek/coreui-free-vue-admin-template)
* GraphQL to save the smart contracts the user want to track

As already said we want to track transactions and Events of the ERC20 Standart, therefore we need a ERC20 ABI
[ERC20 ABI](https://github.com/district0x/district0x-network-token/blob/master/resources/public/contracts/build/ERC20.abi)

## Contributors

* [Mazz](https://gitlab.com/MazzMazz) - Mathias Maier
* [Woif](https://gitlab.com/woifiEssl) - Wolfgang Eßl


## Start App
```
npm install
npm run dev
```

Server should run -> http://localhost:8080

## Start GraphQL Server

```
cd graphqlServer
yarn install
npm run dev
```

Server should run -> http://localhost:4000/graphql

## Start Tests
You can start two tests
- unit tests with jest 
- e2e tests with nightwatch


```
npm run test
npm run e2e 
```

